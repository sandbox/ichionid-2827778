CONTENTS OF THIS FILE
---------------------

 * Introduction
 --------------
  This is a drupal 8 module. It's purpose is to provide a toggle functionality via a double click of "d"

 * Installation
 --------------
   Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure
   for further information.

 * Configuration
 ---------------
   The module has no menu or modifiable settings. There is no configuration.

 * Maintainers
 -------------

   The maintainers of this module are:
    - https://www.drupal.org/u/igorszyporyn
    - https://www.drupal.org/u/ichionid